'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  BASE_URL: "'https://dccustomerapi.azurewebsites.net/'",
  BASE_URL_API: "'https://dccustomerapi.azurewebsites.net/api/'",
  INACTIVE_LOGOUT_TIME: 1200000,
  SESSION_EXPIRED_MESSAGE:"'Your session has expired. Please login to continue'",
  CASH_CUSTOMER_ID: 2,
  CREDIT_CUSTOMER_ID: 3,
  CALL_CENTER_ID: 4,
  FINANCE_ADMIN_ID: 5,
  PORTAL_ADMIN_ID: 1,
  BLOCKED_ADMIN_MESSAGE:"'You have read access only. Kindly contact the admin'",
  DEFAULT_DELIVERY_TIME: "'6:00 am'",
  CUSTOMER_EVIDENCE_OF_PAYMENT_MAXIMUM_FILE_UPLOAD_SIZE: 1024,
  ORDER_STATUS_PROCESSING: 7,
  ORDER_STATUS_CANCELLED: 4,
  ORDER_STATUS_DECLINED: 3,
  ORDER_STATUS_SUCCESSFUL: 2,
  ORDER_STATUS_PENDING: 1,
  ORDER_STATUS_ADMIN_INPUT: 5,
  ORDER_STATUS_CUSTOMER_INPUT: 6,
  ORDER_STATUS_DECLINED_SAP: 9,
  ORDER_STATUS_FAILED_SAP: 8,
  DECLINE_ORDER_NOTES: ["'System Malfunction'","'Wrong Order'","'Insufficient Funds'","'Product Unavailability'"],
  CUSTOMER_DECLINE_ORDER_NOTES: ["'Accidental Request'","'Wrong Delivery Information'","'Insufficient Funds'","'Wrong Product'"]

})
