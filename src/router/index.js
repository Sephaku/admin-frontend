import Vue from 'vue'
import Router from 'vue-router'
import Home from '../Views/Home'
// Error Components
import PageNotFound from '@/Views/Errors/PageNotFound'
import ServerError from '@/Views/Errors/ServerError'

// middleware
import {store} from '../Store'

import orderManagement from '../Views/order/index.vue'
import SourcingList from '../components/Order/SourcingList'

// customer Views
import userManagement from '../Views/customer'
import CustomerManageOrder from '../Views/customer/ManageOrder'
import CustomerDetails from '../Views/customer/details'
import CustomerOrderDetail from '../Views/customer/ManageOrder/details'

// import orderDetails from '../components/Order/orderDetails'
import orderDetails from '../Views/order/detail'

import approveOrder from '../components/Order/approveOrder'
import Analytics from '../Views/analytics/Analytics'
import Adminsettings from '../Views/admin'
import AdminDetails from '../Views/admin/details'
import addAdmin from '../Views/admin/add'
import CreateOrder from '../Views/customer/CreateOrder'
// Layout
import App from '@/Views/Layouts/App'

// Auth views
import Login from '../Views/account/Auth/Login'
import RegistrationSuccess from '../Views/account/RegistrationSuccess'
import firstLogin from '../Views/account/FirstTimeLogin/firstLogin'
import ForgetPassword from '../Views/account/Auth/ForgotPassword'
import ResetPassword from '../Views/account/Auth/ResetPassword/ResetPassword'
import Register from '../Views/account/Auth/Register'

// Messages
import MessageIndex from '../Views/Message'
import MessageView from '../Views/Message/detail'
import MessageCreate from '../Views/Message/create'

import Account from '@/Views/account/Index'

// Notifications
import Notification from '../Views/Notification'

// account
import Profile from '@/Views/account/Profile/Profile'
Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'app',
      component: App,
      children: [{
        path: 'profile',
        name: 'profile',
        component: Profile
      },
      {
        path: '/',
        name: 'dashboard',
        component: Home
      },
      {
        path: '/dashboard',
        name: 'dashboard',
        component: Home
      },
      {
        path: 'order',
        name: 'order',
        component: orderManagement
      },
      {
        path: 'customers',
        name: 'customer.index',
        component: userManagement
      },
      {
        path: 'customer/:id',
        name: 'customer.show',
        component: CustomerDetails
      },
      {
        path: 'customer/create-order/:id',
        name: 'customer.order.create',
        component: CreateOrder,
        beforeEnter: (to, from, next) => {
          const callCenterAgentType = process.env.CALL_CENTER_ID
          const portalAdminIType = process.env.PORTAL_ADMIN_ID
          const authenticatedUserType = store.getters['authentication/userType']
          const allowedValues = [callCenterAgentType, portalAdminIType]
          if (!allowedValues.includes(authenticatedUserType)) {
            return next({
              path: '/'
            })
          }
          return next()
        }
      },
      {
        path: 'customer/manage-order/:id',
        name: 'customer.order.manage',
        component: CustomerManageOrder
      },
      {
        path: 'customer/:customerId/manage-order/detail/:id',
        name: 'customer.order.detail',
        component: CustomerOrderDetail
      },
      {
        path: 'order/sourcing-list',
        name: 'order.SourcingList',
        component: SourcingList
      },
      {
        path: '/message',
        name: 'messages',
        component: MessageIndex
      },
      {
        path: '/message/create',
        name: 'message.create',
        component: MessageCreate
      },
      {
        path: '/message/:id',
        name: 'message.detail',
        component: MessageView
      },
      {
        path: '/notification',
        name: 'notification',
        component: Notification
      },
      {
        path: 'order/:id',
        name: 'orderDetails',
        props: true,
        component: orderDetails
      },
      {
        path: 'orders/:id',
        name: 'orderDetails',
        props: true,
        component: orderDetails
      },
      {
        path: 'approveOrder/:id',
        name: 'approveOrder',
        props: true,
        component: approveOrder
      },
      {
        path: 'Analytics',
        name: 'Analytics',
        component: Analytics
      },
      {
        path: 'admin',
        name: 'admin.setting',
        component: Adminsettings
      },
      {
        path: 'admin/create',
        name: 'admin.create',
        component: addAdmin
      },
      {
        path: 'admin/:id',
        name: 'admin.view',
        component: AdminDetails
      }
      ]
    },
    {
      path: '/',
      name: 'account',
      component: Account,
      children: [{
        path: 'register',
        name: 'register',
        component: Register
      },
      {
        path: 'registration-success',
        name: 'registrationSuccess',
        component: RegistrationSuccess
      },
      {
        path: 'login',
        name: 'login',
        component: Login
      },
      {
        path: 'first-time-login',
        name: 'first-login',
        component: firstLogin
      },
      {
        path: 'reset-password',
        name: 'resetPassword',
        component: ResetPassword
      },
      {
        path: 'forget-password',
        name: 'forgetPassword',
        component: ForgetPassword
      }
      ]
    },
    {
      path: '/',
      component: App,
      children:
        [
          {
            path: '/server-error',
            name: 'ServerError',
            component: ServerError
          },
          {
            path: '*',
            name: 'PageNotFound',
            component: PageNotFound
          }
        ]
    }
  ]
})
router.beforeEach((to, from, next) => {
  const publicPages = ['/login', '/register', '/reset-password', '/forget-password']
  const authRequired = !publicPages.includes(to.path)
  const loggedIn = window.localStorage.getItem('user')
  if (authRequired && !loggedIn) {
    return next('/login')
  }
  next()
})
export default router
// TODO create guard for first time login
