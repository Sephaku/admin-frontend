// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { store } from './Store'
import Element from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import VeeValidate from 'vee-validate'
import Snotify from 'vue-snotify'
import 'vue-snotify/styles/material.css'
import VueMoment from 'vue-moment'
import IdleVue from 'idle-vue'

// dotenv.config()

Vue.use(VeeValidate)
Vue.use(Snotify)
Vue.use(VueMoment)
Vue.use(Element, { size: 'small', zIndex: 3000, locale })

Vue.config.productionTip = false

const eventsHub = new Vue()

Vue.use(IdleVue, {
  eventEmitter: eventsHub,
  idleTime: process.env.INACTIVE_LOGOUT_TIME
})
/**
 * Filter to format currency
 */
Vue.filter('ZAR', function (value) {
  return 'ZAR ' + parseFloat(value).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')

})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
