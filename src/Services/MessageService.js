import Api from './Api'

export const MessageService = {
  get,
  getAll,
  sendNewMessage,
  replyThread,
  deleteMessage,
  deleteMessageThread
}

/**
 * Delete a message thread
 * @param id
 * @returns {Promise<unknown>}
 */
function deleteMessageThread (id) {
  return new Promise((resolve, reject) => {
    return Api.delete(`/Messages/threads/${id}`)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function deleteMessage (id) {
  return new Promise((resolve, reject) => {
    return Api.delete(`/Messages/${id}`)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function replyThread (id, data) {
  return new Promise((resolve, reject) => {
    let config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    return Api.post(`/Messages/threads/${id}/respond`, data, config)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function sendNewMessage (data) {
  return new Promise((resolve, reject) => {
    return Api.post('/Messages/create_message', data)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function get (id) {
  return new Promise((resolve, reject) => {
    return Api.get(`/Messages/threads/${id}`)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function getAll (data) {
  console.info('all', data)
  return new Promise((resolve, reject) => {
    return Api.get(`/Messages/threads?pageNo=${data.pageNo}&searchWord=${data.searchWord}`)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
