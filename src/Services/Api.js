import axios from 'axios'
import router from '../router'
import {store} from '../Store'

const Api = axios.create({
  baseURL: process.env.BASE_URL_API,
  withCredentials: false,
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'access-control-expose-headers': '*'
  },
  // timeout: 2
})
Api.interceptors.response.use(function (response) {
  if (response.data.activestatus === false) {
    let message = {}
    message.custom = process.env.BLOCKED_ADMIN_MESSAGE
    store.dispatch('alert/error', message)
    store.dispatch('authentication/setStatus', response.data.activestatus)
  }
  return response
}, function (error) {
  if (error.response.status === 401) {
    store.dispatch('authentication/logOut')
  }
  return Promise.reject(error)
})

Api.interceptors.request.use(
  (config) => {
    let token = localStorage.getItem('token')
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`
    }
    store.dispatch('alert/clear')
    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

Api.interceptors.response.use(function (response) {
  return response
}, function (error) {
  if (error) {
    console.log('error wahal', error.code)
  }
  if (error.response.status === 401) {
    store.dispatch('authentication/logOut')
    router.push('/login')
    store.dispatch('alert/error', {custom: 'Login to view this area'})
  }
  if (error.response.status === 403) {
    store.dispatch('alert/error', {custom: 'You have been blocked. Please contact the admin'})
  }
  return Promise.reject(error)
})
export default Api
