import Api from './Api'

export const QuoteService = {
  getAll,
  getFilters
}
function getAll (pageNo, searchTerm = null, filterId = null, userId) {
  return new Promise((resolve, reject) => {
    return Api.get('/quotes', {
      params: {
        pageNo,
        searchTerm,
        filterId,
        userId
      }
    })
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function getFilters () {
  return new Promise((resolve, reject) => {
    return Api.get('/quotes/filter-param')
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
