import Api from './Api'

export const NotificationService = {
  markAsRead
}

function markAsRead (id) {
  return new Promise((resolve, reject) => {
    return Api.post(`/notifications/read-alerts?id=${id}`)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
