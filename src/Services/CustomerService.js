import Api from './Api'
import {AdminService} from './AdminService'

export const CustomerService = {
  getAllCustomers,
  getCustomer,
  getRoles,
  blockCustomer,
  unBlockCustomer,
  getCustomersBalance,
  getCustomerAddress
}
function getCustomerAddress (userId) {
  return new Promise((resolve, reject) => {
    return Api.get(`/users/${userId}/addresses`)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function getCustomersBalance (id) {
  return new Promise((resolve, reject) => {
    return Api.get('/users/get-customer-account-balance?userId=' + id)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function blockCustomer (password, id) {
  return new Promise((resolve, reject) => {
    return Api.put('/users/block/' + id, {
      password
    })
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function unBlockCustomer (password, id) {
  return new Promise((resolve, reject) => {
    return Api.put('/users/unblock/' + id, {
      password
    })
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function getRoles () {
  return new Promise((resolve, reject) => {
    return Api.get('/users/get_user_roles')
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getAllCustomers (data) {
  // @Todo remove this method
  return new Promise((resolve, reject) => {
    return Api.get('/users', { params: data })
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getCustomer (id) {
  return new Promise((resolve, reject) => {
    return Api.get('/users/profile/' + id, {
      params: {
        id
      }
    })
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
