import Api from './Api'

export const AdminService = {
  getAllAdmins,
  getAdmin,
  deleteAdmins,
  blockAdmin,
  unBlockAdmin,
  searchAdmins,
  getRoles,
  createAdmin,
  updateAdmin,
  resetPassword
}

function resetPassword (data) {
  return new Promise((resolve, reject) => {
    Api.post('/administrators/reset-password', data)
      .then((response) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
  })
}

function updateAdmin (admin) {
  return new Promise((resolve, reject) => {
    Api.put(`/administrators/${admin.adminId}`, admin)
      .then((response) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
  })
}

function getAllAdmins (data) {
  // @Todo remove this method
  return new Promise((resolve, reject) => {
    return Api.get('/administrators', { params: data })
      .then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
  })
}

function getRoles () {
  return new Promise((resolve, reject) => {
    return Api.get('/administrators/roles')
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function createAdmin (data) {
  return new Promise((resolve, reject) => {
    return Api.post('/administrators', data)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function getAdmin (id) {
  return new Promise((resolve, reject) => {
    return Api.get('/administrators/' + id)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function searchAdmins (searchWord) {
  // @Todo remove this method
  return new Promise((resolve, reject) => {
    return Api.get('/Administrator/search_for_admin_user/' + searchWord)
      .then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
  })
}

function deleteAdmins (newIds) {
  return new Promise((resolve, reject) => {
    Api.post('/administrators/delete', newIds)
      .then((response) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
  })
}
function unBlockAdmin (data) {
  return new Promise((resolve, reject) => {
    Api.put(`/administrators/unblock/${data.id}`, {
      password: data.model.password
    })
      .then((response) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
  })
}
function blockAdmin (data) {
  return new Promise((resolve, reject) => {
    Api.put(`/administrators/block/${data.id}`, {
      password: data.model.password,
      comment: data.model.comment
    })
      .then((response) => {
        resolve(response)
      })
      .catch((error) => {
        reject(error)
      })
  })
}
