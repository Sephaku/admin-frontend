import Api from './Api'

export const orderService = {
  getOrders,
  getSourcingList,
  getOrderDetails,
  getActiveAddresses,
  getPalletInformation,
  placeOrder,
  getMoffetsInformation,
  getAll,
  getFilters,
  getAllCustomersOrders,
  get,
  cancelCustomerOrder,
  exportOrder,
  getDashboardData,
  deleteFile,
  approveOrder
}
function approveOrder (order) {
  return new Promise((resolve, reject) => {
    return Api.post(`/orders/Approve/`, order)
      .then((response) => {
        resolve(response)
      }).catch((error) => {
        reject(error)
      })
  })
}
function deleteFile (fileName) {
  return new Promise((resolve, reject) => {
    return Api.delete(`/orders/delete-file/${fileName}`)
      .then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]))
        const link = document.createElement('a')
        link.href = url
        link.setAttribute('download', 'Order-details.pdf')
        document.body.appendChild(link)
        link.click()
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function exportOrder (id) {
  return new Promise((resolve, reject) => {
    return Api.get(`/orders/pdf/${id}`)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function cancelCustomerOrder (orderId, message) {
  return new Promise((resolve, reject) => {
    return Api.post('/orders/cancel', {id: orderId, comment: message})
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function get (id, customerId = null) {
  return new Promise((resolve, reject) => {
    if (customerId === null) {
      customerId = 0
    }
    return Api.get('/orders/' + id + `?userId=${customerId}`)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function getAllCustomersOrders (params) {
  // @Todo remove this method
  return new Promise((resolve, reject) => {
    Api.get('/orders/', {
      params
    })
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function getFilters (customerId = '') {
  let userId = customerId
  // @Todo remove this method
  return new Promise((resolve, reject) => {
    Api.get(`/orders/filter-param?userId=${userId}`)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function getAll (params) {
  // @Todo remove this method
  return new Promise((resolve, reject) => {
    Api.get('/orders', {
      params
    })
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function placeOrder (order) {
  // @Todo remove this method
  return new Promise((resolve, reject) => {
    Api.post('/orders/create/admin', order)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getOrders () {
  return new Promise((resolve, reject) => {
    return Api.post('orders/get-orders')
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getSourcingList () {
  return new Promise((resolve, reject) => {
    return Api.get('/administrators/sourcing-list')
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getOrderDetails (id, userId) {
  return new Promise((resolve, reject) => {
    return Api.get('/quotes/' + id + '?userId=' + userId)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}

function getActiveAddresses (id) {
  return new Promise((resolve, reject) => {
    return Api.get(`/products/${0}/addresses/` + id)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}

/**
 * fetch pallet information from server
 * @param quantity
 * @returns {Promise<unknown>}
 */
function getPalletInformation (quantity) {
  return new Promise((resolve, reject) => {
    return Api.get('/products/pallets/' + quantity)
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject( error)
      })
  })
}

function getDashboardData () {
  return new Promise((resolve, reject) => {
    Api.get('/administrators/dashboard').then((response) => {
      resolve(response)
    }).catch((error) => {
      reject(error)
    })
  })
}

function getMoffetsInformation () {
  return new Promise((resolve, reject) => {
    return Api.get('/products/moffats')
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
