import Api from './Api'
import swal from 'sweetalert'

export const userService = {
  login,
  logout,
  forgetPassword,
  changePassword,
  resetPassword,
  newAccountChangePassword
}

function login (email, password, rememberMe) {
  return new Promise((resolve, reject) => {
    return Api.post('/administrators/authenticate', {
      emailAddress: email,
      password: password,
      rememberMe: rememberMe
    })
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function resetPassword (passwordConfirmation, password, token) {
  return new Promise((resolve, reject) => {
    return Api.post('/administrators/reset-password', {
      token: token,
      newPassword: password,
      confirmPassword: passwordConfirmation
    }).then(response => {
      resolve(response)
    }).catch(error => {
      reject(error)
    })
  })
}
function changePassword (oldPassword, newPassword, confirmPassword) {
  return new Promise((resolve, reject) => {
    return Api.post('/users/change-password', {
      oldPassword: oldPassword,
      newPassword: newPassword,
      confirmPassword: confirmPassword
    }).then(response => {
      resolve(response)
    }).catch(error => {
      reject(error)
    })
  })
}
function logout () {
  window.localStorage.clear()
  delete Api.defaults.headers.common['Authorization']
}

function forgetPassword (email) {
  return new Promise((resolve, reject) => {
    return Api.post('/users/forgot-password', {
      emailAddress: email
    })
      .then(response => {
        resolve(response)
      })
      .catch(error => {
        reject(error)
      })
  })
}
function newAccountChangePassword (passwordConfirmation, password) {
  return new Promise((resolve, reject) => {
    return Api.post('/users/first-login-reset-password', {
      newPassword: password,
      confirmPassword: passwordConfirmation
    }).then(response => {
      resolve(response)
    }).catch(error => {
      reject(error)
    })
  })
}
