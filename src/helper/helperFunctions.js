/**
 * Application Helper functions
 */

/**
 * base64ToArrayBuffer
 * @param base64
 * @returns {Uint8Array}
 */
export function base64ToArrayBuffer (base64) {
  let binaryString = window.atob(base64)
  let binaryLen = binaryString.length
  let bytes = new Uint8Array(binaryLen)
  for (let i = 0; i < binaryLen; i++) {
    bytes[i] = binaryString.charCodeAt(i)
  }
  return bytes
}
// Save bytes as an array
export function saveByteArray (reportName, byte) {
  let blob = new Blob([byte], {type: 'application/pdf'})
  let link = document.createElement('a')
  link.href = window.URL.createObjectURL(blob)
  link.download = reportName
  link.click()
}
// Download a blob file from API
export function downloadBlob (response) {
  const url = window.URL.createObjectURL(new Blob([response.data]))
  const link = document.createElement('a')
  link.href = url
  link.setAttribute('download', process.env.EVIDENCE_OF_PAYMENT_FILE_NAME)
  document.body.appendChild(link)
  link.click()
}
// format currency
export function formatCurrency (amount) {
  return parseFloat(amount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
}
// Scroll to top
export function scrollToTop () {
  window.scrollTo(0, 0)
}
/**
 * Validate the size of a file
 * @param file
 */
export function validateFileUploadSize (file) {
  return file.size <=
    process.env.CUSTOMER_EVIDENCE_OF_PAYMENT_MAXIMUM_FILE_UPLOAD_SIZE *
    process.env.CUSTOMER_EVIDENCE_OF_PAYMENT_MAXIMUM_FILE_UPLOAD_SIZE
}

/**
 *
 * @param status
 * @returns {string}
 */
export function generateStatusColor (status) {
  const successCodes = [process.env.ORDER_STATUS_SUCCESSFUL]
  const warningCodes = [
    process.env.ORDER_STATUS_PENDING,
    process.env.ORDER_STATUS_ADMIN_INPUT,
    process.env.ORDER_STATUS_CUSTOMER_INPUT
  ]
  const processing =
    [
      process.env.ORDER_STATUS_PROCESSING
    ]
  const errorCodes = [
    process.env.ORDER_STATUS_DECLINED,
    process.env.ORDER_STATUS_CANCELLED,
    process.env.ORDER_STATUS_DECLINED_SAP,
    process.env.ORDER_STATUS_FAILED_SAP
  ]
  if (successCodes.includes(status)) {
    return '#00BA41'
  }
  if (warningCodes.includes(status)) {
    return '#FF7F00'
  }
  if (errorCodes.includes(status)) {
    return '#E60000'
  }
  return '#0076C2'
}

/**
 * Check if a customer order can be cancelled
 * @param orderStatus
 * @param customerType
 * @returns {boolean}
 */
export function canCancelCustomerOrder (orderStatus, customerType) {
  if (
    orderStatus === process.env.ORDER_STATUS_PROCESSING ||
    orderStatus === process.env.ORDER_STATUS_CANCELLED ||
    orderStatus === process.env.ORDER_STATUS_DECLINED ||
    orderStatus === process.env.ORDER_STATUS_SUCCESSFUL
  ) {
    return true
  }
  if (customerType) {
    if (orderStatus === 1) {
      return true
    }
  }
  return false
}
