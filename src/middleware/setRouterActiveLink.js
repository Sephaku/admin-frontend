import {store} from '../Store'

export default (to, from, next) => {
  const callCenterAgentType = process.env.CALL_CENTER_ID
  const portalAdminIType = process.env.PORTAL_ADMIN_ID
  const authenticatedUserType = store.getters['authentication/userType']
  const allowedValues = [callCenterAgentType, portalAdminIType]
  if (!allowedValues.includes(authenticatedUserType)) {
    return next({
      path: '/'
    })
  }
  return next()
}
