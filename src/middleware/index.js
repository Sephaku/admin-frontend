import router from '../router'

export default function index ({ next }) {
  let isNewUser = this.$store.state.authentication.user.isFirstTimeLogin
  if (isNewUser) {
    return router.push({ name: '/' })
  }
  return next()
}
