import {
  CustomerService
} from '../Services'
export const customers = {
  namespaced: true,
  state: {
    customers: [],
    activeCustomer: false,
    roles: [],
    balance: '',
    customerAddresses: [],
    loading: false
  },
  getters: {
    isCashCustomer: state => state.activeCustomer.roleId === process.env.CASH_CUSTOMER_ID,
    isCreditCustomer: state => state.activeCustomer.roleId === process.env.CREDIT_CUSTOMER_ID
  },
  actions: {
    getCustomerAddress ({ commit, dispatch }, { userId }) {
      commit('LOADING')
      return new Promise((resolve) => {
        CustomerService.getCustomerAddress(userId)
          .then(response => {
            commit('SET_CUSTOMER_ADDRESSES', response.data.result)
            resolve(response.data)
            commit('LOADING')
          }).catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
          })
      })
    },
    getCustomersBalance ({commit, dispatch}, {customerId}) {
      return new Promise(() => {
        commit('LOADING')
        CustomerService.getCustomersBalance(customerId)
          .then(response => {
            commit('SET_CUSTOMER_BALANCE', response.data.result)
            commit('LOADING')
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
          })
      })
    },
    blockCustomer ({commit, dispatch}, {password, id}) {
      commit('LOADING')
      return new Promise((resolve, reject) => {
        CustomerService.blockCustomer(password, id)
          .then(response => {
            resolve(response.data)
            commit('SET_BLOCK_CUSTOMER', response.data.result)
            commit('LOADING')
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
          })
      })
    },
    unBlockCustomer ({commit, dispatch}, {password, id}) {
      commit('LOADING')
      return new Promise((resolve, reject) => {
        CustomerService.unBlockCustomer(password, id)
          .then(response => {
            resolve(response.data.result)
            commit('SET_UNBLOCK_CUSTOMER', response.data.result)
            commit('LOADING')
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
          })
      })
    },
    getAllCustomers ({commit, dispatch}, {data}) {
      commit('LOADING')
      return new Promise((resolve) => {
        CustomerService.getAllCustomers(data)
          .then(response => {
            resolve(true)
            commit('SET_CUSTOMERS', response.data.result)
            commit('LOADING')
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
          })
      })
    },
    getCustomer ({commit, dispatch}, {id}) {
      commit('LOADING')
      return new Promise((resolve, reject) => {
        CustomerService.getCustomer(id)
          .then(response => {
            commit('SET_ACTIVE_CUSTOMER', response.data.result)
            resolve(true)
            commit('LOADING')
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
          })
      })
    },
    getRoles ({commit, dispatch }) {
      commit('LOADING')
      return new Promise((resolve) => {
        CustomerService.getRoles()
          .then(response => {
            resolve(response.data.result)
            commit('SET_CUSTOMER_ROLES', response.data.result)
            commit('LOADING')
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
          })
      })
    }
  },
  mutations: {
    LOADING (state) {
      state.loading = !state.loading
    },
    SET_CUSTOMER_ADDRESSES (state, addresses) {
      addresses.forEach(address => {
        address.value = address.description
      })
      state.customerAddresses = addresses
    },
    SET_CUSTOMERS (state, data) {
      state.customers = data
    },
    SET_ACTIVE_CUSTOMER (state, data) {
      state.activeCustomer = data
    },
    SET_CUSTOMER_ROLES (state, roles) {
      state.roles = roles
    },
    SET_BLOCK_CUSTOMER (state) {
      state.activeCustomer.is_blocked = true
    },
    SET_UNBLOCK_CUSTOMER (state) {
      state.activeCustomer.is_blocked = false
    },
    SET_CUSTOMER_BALANCE (state, data) {
      state.balance = data.creditBalance
    }
  }
}
