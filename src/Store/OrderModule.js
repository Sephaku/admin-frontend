import {
  orderService
} from '../Services'
import {base64ToArrayBuffer, saveByteArray} from '../helper/helperFunctions'

export const orders = {
  namespaced: true,
  state: {
    dashboardData: {},
    loading: false,
    buttonLoader: false,
    details: {
      statusCode: 0,
      cartDetail: []
    },
    orders: [],
    customerOrders: [],
    filters: [],
    sourcingList: [],
    ActiveOrderDetails: false,
    activeAddresses: [],
    activePalletInformation: false,
    activeMoffetsInformation: false
  },
  actions: {
    approveOrder ({commit, dispatch}, {order}) {
      commit('LOADING')
      return new Promise((resolve) => {
        orderService.approveOrder(order)
          .then((response) => {
            commit('LOADING')
            resolve(response)
          }).catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
          })
      })
    },
    deleteFile ({commit, dispatch}, {fileName}) {
      commit('LOADING')
      return new Promise((resolve) => {
        orderService.deleteFile(fileName)
          .then(response => {
            commit('SET_DASHBOARD_DATA', response.data.result)
            commit('LOADING')
            resolve(response.data.message)
          }).catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
          })
      })
    },
    getDashboardData ({commit, dispatch}) {
      commit('LOADING')
      return new Promise((resolve) => {
        orderService.getDashboardData()
          .then(response => {
            commit('SET_DASHBOARD_DATA', response.data.result)
            commit('LOADING')
            resolve(true)
          }).catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
          })
      })
    },
    exportOrder  ({commit, dispatch}, {id}) {
      commit('LOADING')
      orderService.exportOrder(id)
        .then((response) => {
          commit('LOADING')
          let FileBytes = response.data.result.fileBytes
          let fileName = response.data.result.fileName
          FileBytes = base64ToArrayBuffer(FileBytes)
          saveByteArray(fileName, FileBytes)
        }).catch(error => {
          dispatch('alert/error', error, { root: true })
          commit('LOADING')
        })
    },
    get ({commit, dispatch}, {id, customerId}) {
      commit('LOADING')
      return new Promise((resolve) => {
        orderService.get(id, customerId)
          .then(response => {
            commit('SET_ACTIVE_ORDER', response.data.result)
            commit('LOADING')
            resolve(true)
          }).catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
          })
      })
    },
    getFilters ({commit, dispatch}, {customerId}) {
      return new Promise((resolve, reject) => {
        orderService.getFilters(customerId)
          .then(response => {
            commit('SET_FILTERS', response.data.result)
            resolve(response)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    getAllCustomersOrders ({commit, dispatch}, {params}) {
      return new Promise((resolve, reject) => {
        orderService.getAllCustomersOrders(params)
          .then(response => {
            commit('SET_CUSTOMERS_ORDERS', response.data.result)
            resolve(response)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    getAll ({commit, dispatch}, {params}) {
      return new Promise((resolve, reject) => {
        orderService.getAll(params)
          .then(response => {
            commit('SET_ORDERS', response.data.result)
            resolve(response)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    placeOrder ({commit, dispatch}, {order}) {
      return new Promise((resolve, reject) => {
        orderService.placeOrder(order)
          .then(response => {
            resolve(response)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            reject(true)
          })
      })
    },
    getMoffetsInformation ({commit, dispatch}) {
      return new Promise((resolve, reject) => {
        orderService.getMoffetsInformation()
          .then(response => {
            commit('SET_ACTIVE_MOFFETS', response.data.result)
            resolve(response)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    getPalletInformation ({commit, dispatch}, {quantity}) {
      return new Promise((resolve, reject) => {
        orderService.getPalletInformation(quantity)
          .then(response => {
            commit('SET_ACTIVE_PALLET', response.data.result)
            resolve(response)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    getActiveAddresses  ({commit, dispatch}, {id}) {
      return new Promise((resolve, reject) => {
        orderService.getActiveAddresses(id)
          .then(response => {
            commit('SET_ACTIVE_ADDRESS', response.data.result)
            resolve(response)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    getOrderDetails ({commit, dispatch}, {id, userId}) {
      return new Promise((resolve) => {
        orderService.getOrderDetails(id, userId)
          .then(response => {
            commit('SET_ACTIVE_ORDER_DETAIL', response.data.result)
            resolve(response)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    getSourcingList ({commit, dispatch}) {
      return new Promise((resolve, reject) => {
        orderService.getSourcingList()
          .then(response => {
            commit('SET_SOURCING_LIST', response.data.result)
            resolve(response)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    cancelCustomerOrder ({commit, dispatch, state}, {message}) {
      commit('SET_BUTTON_LOADER')
      let orderId = state.details.poNumber
      return new Promise((resolve) => {
        orderService.cancelCustomerOrder(orderId, message)
          .then(response => {
            commit('SET_BUTTON_LOADER')
            resolve(response.data.message)
          }).catch(error => {
            commit('SET_BUTTON_LOADER')
            dispatch('alert/error', error, { root: true })
          })
      })
    }
  },
  mutations: {
    SET_DASHBOARD_DATA (state, dashboardData) {
      state.dashboardData = dashboardData
    },
    SET_SOURCING_LIST (state, sourcingList) {
      state.sourcingList = sourcingList
    },
    SET_ACTIVE_ORDER_DETAIL (state, orderDetail) {
      state.ActiveOrderDetails = orderDetail
    },
    SET_ACTIVE_ADDRESS (state, addresses) {
      addresses.forEach(address => {
        address.value = address.description
      })
      state.activeAddresses = addresses
    },
    SET_ACTIVE_PALLET (state, pallet) {
      state.activePalletInformation = pallet
    },
    SET_ACTIVE_MOFFETS (state, moffets) {
      state.activeMoffetsInformation = moffets
    },
    SET_ORDERS (state, orders) {
      state.orders = orders
    },
    SET_FILTERS (state, filters) {
      state.filters = filters
    },
    SET_CUSTOMERS_ORDERS (state, orders) {
      state.customerOrders = orders
    },
    SET_ACTIVE_ORDER (state, order) {
      state.details = order
    },
    LOADING (state) {
      state.loading = !state.loading
    },
    SET_BUTTON_LOADER (state) {
      state.buttonLoader = !state.buttonLoader
    }
  }
}
