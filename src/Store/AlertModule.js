// Dependencies
import Router from '../router'

export const alert = {
  namespaced: true,
  state: {
    type: null,
    message: null
  },
  actions: {
    success ({ commit }, message) {
      commit('success', message)
    },
    error ({ commit }, message) {
      commit('error', message)
    },
    clear ({ commit }) {
      commit('clear')
    },
    updateBalance ({commit}, amount) {
      commit('updateBalance', amount)
    }
  },
  mutations: {
    success (state, message) {
      state.type = 'success'
      state.message = message
    },
    error (state, message) {
      if (message.response.status === 404) {
        Router.push('/404')
        return
      }
      if (message.response) {
        window.scrollTo(0, 0)
        state.type = 'error'
        if (message.response.data.errors) {
          let errors = message.response.data.errors
          let errorMessage = ''
          for (let key in errors) {
            if (errors.hasOwnProperty(key)) {
              let message = `${errors[key]}` + '\n'
              errorMessage += message
            }
          }
          state.message = errorMessage
          return
        }
        if (message.response.data.message) {
          state.message = message.response.data.message
          return
        }
        if (message.custom) {
          state.message = message.custom
          return
        }
        return
      }
      if (message.custom) {
        state.message = message.custom
        return
      }
      Router.push('/server-error')
    },
    clear (state) {
      state.type = null
      state.message = null
    }
  }
}
