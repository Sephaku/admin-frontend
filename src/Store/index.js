import Vue from 'vue'
import Vuex from 'vuex'
import { alert } from './AlertModule'
import { authentication } from './AuthenticationModule'
import { users } from './UsersModule'
import { notification } from './NotificationModule'
import { orders } from './OrderModule'
import { admin } from './AdminModule'
import { customers } from './CustomerModule'
import { quotes } from './QuoteModule'
import { messages } from './MessageModule'
import { sideMenu } from './SideMenuModule'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    alert,
    authentication,
    users,
    notification,
    orders,
    admin,
    customers,
    quotes,
    messages,
    sideMenu
  }
})
