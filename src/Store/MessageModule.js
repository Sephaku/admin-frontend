import {MessageService} from '../Services'
export const messages = {
  namespaced: true,
  state: {
    loading: false,
    messages: false,
    message: false
  },
  actions: {
    deleteMessageThread ({commit, dispatch}, {threadId}) {
      commit('LOADING')
      return new Promise((resolve) => {
        MessageService.deleteMessageThread(threadId)
          .then(response => {
            commit('LOADING')
            resolve(response.data.msg)
          }).catch(error => {
            commit('LOADING')
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    deleteMessage ({commit, dispatch}, {id}) {
      commit('LOADING')
      return new Promise((resolve, reject) => {
        MessageService.deleteMessage(id)
          .then(response => {
            commit('LOADING')
            resolve(response.data.msg)
          }).catch(error => {
            commit('LOADING')
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    get ({commit, dispatch}, {id}) {
      return new Promise((resolve, reject) => {
        MessageService.get(id)
          .then(response => {
            commit('SET_MESSAGE', response.data.result)
            resolve(response.data.result)
          }).catch(error => {
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    getAll ({commit, dispatch}, {data}) {
      commit('LOADING')
      data = data || {pageNo: 1, searchWord: '' }
      return new Promise((resolve, reject) => {
        MessageService.getAll(data)
          .then(response => {
            commit('SET_MESSAGES', response.data.result)
            commit('LOADING')
            resolve(true)
          }).catch(error => {
            commit('LOADING')
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    sendNewMessage ({commit, dispatch}, {data}) {
      commit('LOADING')
      return new Promise((resolve, reject) => {
        MessageService.sendNewMessage(data)
          .then(response => {
            commit('LOADING')
            resolve(response.data.message)
          }).catch(error => {
            commit('LOADING')
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    replyThread ({commit, dispatch}, {id, data}) {
      commit('LOADING')
      return new Promise((resolve) => {
        MessageService.replyThread(id, data)
          .then(response => {
            commit('LOADING')
            resolve(response.data.result)
          }).catch(error => {
            commit('LOADING')
            dispatch('alert/error', error, { root: true })
          })
      })
    }
  },
  mutations: {
    SET_MESSAGES (state, messages) {
      state.messages = messages
    },
    SET_MESSAGE (state, message) {
      state.message = message
    },
    LOADING (state) {
      state.loading = !state.loading
    }
  },
  getters: {}
}
