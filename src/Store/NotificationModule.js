import {NotificationService} from '../Services'

const signalR = require('@aspnet/signalr')

export const notification = {
  namespaced: true,
  state: {
    newNotifications: [],
    notificationCounter: 0,
    messageCount: 0,
    all: []
  },
  actions: {
    openConnection ({commit, dispatch}) {
      let connection = new signalR.HubConnectionBuilder()
        .withUrl(process.env.BASE_URL + '/notifications', { accessTokenFactory: () => localStorage.getItem('token') })
        .configureLogging(signalR.LogLevel.Information)
        .build()
      connection.start().catch(function (err) {
        dispatch('alert/error', err, { root: true })
      })
      connection.on('get-message-count', function (notifications) {
        commit('MESSAGE_COUNT', JSON.parse(notifications))
      })
      connection.on('get-new-alerts', function (notifications) {
        commit('SET_NEW_NOTIFICATION', JSON.parse(notifications))
        commit('SET_NOTIFICATION_COUNTER')
      })
      connection.on('get-list-of-alerts', function (notifications) {
        commit('SET_NOTIFICATION', JSON.parse(notifications))
      })
    },
    resetNotificationCounter ({commit}) {
      commit('RESET_NOTIFICATION_COUNTER')
    },
    markAsRead ({commit, dispatch}, {id}) {
      return new Promise((resolve, reject) => {
        NotificationService.markAsRead(id)
          .then(response => {
          }).catch(error => {
            dispatch('alert/error', error, { root: true })
            reject(error)
          })
      })
    }
  },
  mutations: {
    MESSAGE_COUNT (state, count) {
      if (count === 0) {
        state.messageCount = null
        return
      }
      state.messageCount = count
    },
    SET_NOTIFICATION (state, notifications) {
      state.all = notifications
    },
    SET_NEW_NOTIFICATION (state, notifications) {
      state.newNotifications = notifications
    },
    RESET_NOTIFICATION_COUNTER (state) {
      state.notificationCounter = 0
    },
    SET_NOTIFICATION_COUNTER (state) {
      if (state.all.length === 0) {
        state.notificationCounter = null
        return
      }
      state.notificationCounter = state.all.length
    }
  },
  getters: {
    newNotificationsCount: state => state.notificationCounter,
    newMessageCount: state => state.messageCount
  }
}
