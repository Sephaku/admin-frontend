import {QuoteService} from '../Services'
export const quotes = {
  namespaced: true,
  state: {
    defaultQuotes: [],
    quoteData: {},
    loading: false,
    filterData: []
  },
  actions: {
    getAll ({ commit, dispatch }, {pageNo = 1, searchTerm, filterId, userId}) {
      commit('LOADING')
      return new Promise((resolve, reject) => {
        QuoteService.getAll(pageNo, searchTerm, filterId, userId)
          .then(response => {
            commit('SET_QUOTE_DATA', response.data.result)
            commit('LOADING')
            resolve(true)
          })
          .catch(error => {
            commit('LOADING')
            dispatch('alert/error', error, { root: true })
          })
      })
    },
    getFilters ({ commit, dispatch }) {
      QuoteService.getFilters()
        .then(response => {
          commit('SET_QUOTE_FILTER_DATA', response.data.result)
        }).catch(error => { dispatch('alert/error', error, { root: true }) })
    }
  },
  mutations: {
    SET_QUOTE_DATA (state, data) {
      state.quoteData = data
    },
    LOADING (state) {
      state.loading = !state.loading
    },
    SET_QUOTE_FILTER_DATA (state, data) {
      state.filterData = data
    }
  },
  getters: {
    quoteData: state => state.quoteData,
    loading: state => state.loading,
    filterValues: state => state.filterData
  }
}
