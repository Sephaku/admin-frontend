import {
  AdminService
} from '../Services'
export const admin = {
  namespaced: true,
  state: {
    admins: [],
    activeAdmin: {},
    roles: [],
    loading: false
  },
  actions: {
    resetPassword ({commit, dispatch}, {data}) {
      commit('LOADING')
      return new Promise((resolve, reject) => {
        AdminService.resetPassword(data)
          .then(response => {
            commit('LOADING')
            resolve(response.data.result)
          })
          .catch(error => {
            commit('LOADING')
            dispatch('alert/error', error, { root: true })
            reject(Error('Failedd'))
          })
      })
    },
    updateAdmin ({commit, dispatch}, {admin}) {
      commit('LOADING')
      return new Promise((resolve, reject) => {
        AdminService.updateAdmin(admin)
          .then(response => {
            commit('LOADING')
            resolve(response.data.result)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
            reject(error(true))
          })
      })
    },
    searchAdmins ({commit, dispatch}, {searchWord}) {
      commit('LOADING')
      return new Promise((resolve, reject) => {
        AdminService.searchAdmins(searchWord)
          .then(response => {
            commit('SET_ADMINS', response.data.result)
            commit('LOADING')
            resolve(true)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
            reject(error(true))
          })
      })
    },
    getAllAdmins ({commit, dispatch}, {data}) {
      return new Promise((resolve, reject) => {
        commit('LOADING')
        AdminService.getAllAdmins(data)
          .then(response => {
            commit('SET_ADMINS', response.data.result)
            commit('LOADING')
            resolve(true)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
            reject(error(true))
          })
      })
    },
    getRoles ({commit, dispatch}) {
      return new Promise((resolve, reject) => {
        commit('LOADING')
        AdminService.getRoles()
          .then(response => {
            commit('LOADING')
            resolve(true)
            commit('SET_ADMIN_ROLES', response.data.result)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
            reject(error(true))
          })
      })
    },
    createAdmin ({commit, dispatch}, {data}) {
      commit('LOADING')
      return new Promise((resolve, reject) => {
        AdminService.createAdmin(data)
          .then(response => {
            commit('LOADING')
            resolve(response.data.msg)
          })
          .catch(error => {
            commit('LOADING')
            dispatch('alert/error', error, { root: true })
            reject(error(true))
          })
      })
    },
    getAdmin ({commit, dispatch}, {id}) {
      return new Promise((resolve, reject) => {
        commit('LOADING')
        AdminService.getAdmin(id)
          .then(response => {
            commit('SET_ACTIVE_ADMIN', response.data.result)
            commit('LOADING')
            resolve(true)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
            reject(error(true))
          })
      })
    },
    deleteAdmins ({commit, dispatch}, {ids}) {
      return new Promise((resolve, reject) => {
        AdminService.deleteAdmins(ids)
          .then(response => {
            resolve(response.data.msg)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            reject(error(true))
          })
      })
    },
    unBlockAdmin ({commit, dispatch}, {data}) {
      return new Promise((resolve, reject) => {
        commit('LOADING')
        AdminService.unBlockAdmin(data)
          .then(response => {
            resolve(response.data.msg)
            commit('LOADING')
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
            reject(error(true))
          })
      })
    },
    blockAdmin ({commit, dispatch}, {data}) {
      return new Promise((resolve, reject) => {
        commit('LOADING')
        AdminService.blockAdmin(data)
          .then(response => {
            resolve(response.data.msg)
            commit('LOADING')
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('LOADING')
            reject(error(true))
          })
      })
    }
  },
  mutations: {
    SET_ADMINS (state, data) {
      state.admins = data
    },
    SET_ACTIVE_ADMIN (state, data) {
      state.activeAdmin = data
    },
    SET_ADMIN_ROLES (state, roles) {
      state.roles = roles
    },
    LOADING (state) {
      state.loading = !state.loading
    }
  }
}
