import Vue from 'vue'
export const sideMenu = {
  namespaced: true,
  state: {
    isCollapse: false,
    activeIndex: '1'
  },
  actions: {
    collapse ({commit}) {
      commit('COLLAPSE')
    },
    setActiveIndex ({commit}, {index}) {
      commit('SET_ACTIVE_INDEX', index)
    }
  },
  mutations: {
    COLLAPSE (state) {
      state.isCollapse = !state.isCollapse
      Vue.set(state, 'isCollapse', state.isCollapse)
    },
    SET_ACTIVE_INDEX (state, index) {
      state.activeIndex = index
      Vue.set(state, 'activeIndex', index)
    }
  }
}
