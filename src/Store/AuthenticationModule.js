import {
  userService
} from '../Services'

const user = JSON.parse(window.localStorage.getItem('user'))
const initialState = user
  ? {
    status: {
      loggedIn: true
    },
    user,
    isActive: true
  }
  : {
    status: {},
    user: null,
    isActive: true
  }

export const authentication = {
  namespaced: true,
  state: initialState,
  getters: {
    isAuthenticated: state => {
      return state.status.loggedIn
    },
    processing: state => {
      return state.status.processing
    },
    userType: state => state.user.userType,
    isPortalAdmin: state => state.user.userType === process.env.PORTAL_ADMIN_ID,
    isCallCenter: state => state.user.userType === process.env.CALL_CENTER_ID,
    isFinanceAdmin: state => {
      if (state.user.userType === process.env.FINANCE_ADMIN_ID) {
        return true
      }
      return false
    },
    userId: state => state.user.id
  },
  actions: {
    setStatus ({commit}, status) {
      commit('SET_ACTIVE_STATUS', status)
    },
    login ({ dispatch, commit }, { email, password, rememberMe }) {
      commit('loginRequest', { email })
      return new Promise((resolve, reject) => {
        userService.login(email, password, rememberMe)
          .then(response => {
            window.localStorage.setItem('user', JSON.stringify(response.data.result))
            window.localStorage.setItem('token', response.data.result.token)
            let user = response.data.result
            commit('loginSuccess', user)
            commit('completed')
            resolve(user)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('completed')
            reject(Error('Login Error!!'))
          })
      })
    },
    logOut ({
      commit,
      dispatch
    }) {
      userService.logout()
      commit('logout')
      localStorage.clear()
      location.reload()
    },
    resetPassword ({ commit, dispatch }, {passwordConfirmation, password, token}) {
      commit('processing')
      return new Promise((resolve, reject) => {
        userService.resetPassword(passwordConfirmation, password, token)
          .then(response => {
            dispatch('alert/success', 'You have successfully changed your password', { root: true })
            resolve(true)
            commit('completed')
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            reject(Error('Login Error!!'))
            commit('completed')
          })
      })
    },
    changePassword ({ commit, dispatch }, {oldPassword, newPassword, confirmPassword}) {
      commit('processing')
      return new Promise((resolve, reject) => {
        userService.changePassword(oldPassword, newPassword, confirmPassword)
          .then(response => {
            resolve(response.data.msg)
            dispatch('alert/success', 'You have successfully changed your password', { root: true })
            commit('completed')
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('completed')
            reject(Error('error'))
          })
      })
    },
    forgetPassword ({ dispatch, commit }, {email}) {
      commit('processing')
      return new Promise((resolve, reject) => {
        userService.forgetPassword(email)
          .then(response => {
            commit('completed')
            dispatch('alert/success', response.data.msg, { root: true })
            resolve(true)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('completed')
          })
      })
    },
    newAccountChangePassword ({commit, dispatch}, { passwordConfirmation, password }) {
      commit('processing')
      return new Promise((resolve, reject) => {
        userService.newAccountChangePassword(passwordConfirmation, password)
          .then(response => {
            commit('completed')
            resolve(response.data.msg)
          })
          .catch(error => {
            dispatch('alert/error', error, { root: true })
            commit('completed')
          })
      })
    }
  },
  mutations: {
    SET_ACTIVE_STATUS (state, status) {
      state.isActive = status
    },
    processing (state) {
      state.status = {
        processing: true
      }
    },
    completed (state) {
      state.status = {
        processing: false
      }
    },
    loginRequest (state, user) {
      state.status = {
        processing: true
      }
      state.user = user
    },
    loginSuccess (state, user) {
      state.status = {
        processing: true
      }
      state.user = user
    },
    loginFailure (state) {
      state.status = {
        processing: false
      }
      state.user = null
    },
    logout (state) {
      state.status = {}
      state.user = null
    }
  }
}
